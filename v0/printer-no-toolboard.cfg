# This file contains common pin mappings for the BIGTREETECH SKR
# MINI 1.2. To use this config, the firmware should be compiled for 
# the STM32F103 with a "28KiB bootloader". Also select "enable extra 
# low-level configuration options" and configure "GPIO pins to set 
# at micro-controller startup" to "!PC13" for a SKR Mini 1.2

# The "make flash" command does not work on the SKR mini. Instead,
# after running "make", copy the generated "out/klipper.bin" file to a
# file named "firmware.bin" on an SD card and then restart the SKR
# mini with that SD card.

# See the example.cfg file for a description of available parameters.

[include mainsail.cfg]
#[include DisplayEncoder.cfg]

[mcu]
##	Obtain mcu value by "ls -l /dev/serial/by-id/" 
serial: /dev/serial0
restart_method: command

[stepper_x]
step_pin: gpio14
dir_pin: !gpio13	# Remove the ! if stepper is moving the opposite direction
enable_pin: !gpio15
endstop_pin: ^gpio4
rotation_distance: 40
microsteps: 16
position_endstop: 120
position_max: 120
homing_speed: 20   #Max 100
homing_retract_dist: 5
homing_positive_dir: true

[tmc2209 stepper_x]
uart_pin: gpio9
tx_pin: gpio8
uart_address: 3
run_current: .25
#sense_resistor: 0.110
stealthchop_threshold: 500

[stepper_y]
step_pin: gpio6
dir_pin: !gpio5
enable_pin: !gpio7
endstop_pin: ^gpio3
rotation_distance: 40
microsteps: 16
position_endstop: 118
position_max: 118
homing_speed: 20   #Max 100
homing_retract_dist: 5
homing_positive_dir: true

[tmc2209 stepper_y]
uart_pin: gpio9
tx_pin: gpio8
uart_address: 2
interpolate: True
run_current: .25
#sense_resistor: 0.110
stealthchop_threshold: 500

[stepper_z]
step_pin: gpio19
dir_pin: !gpio28
enable_pin: !gpio2
endstop_pin: probe:z_virtual_endstop
rotation_distance: 8 #for T8x8 lead screw
#rotation_distance: 2 #for T8x2 lead screw
microsteps: 16
#home with probe
#endstop_pin: PC2
#position_endstop: -.10
position_max: 120
position_min: -1
homing_speed: 5   #Max 100
second_homing_speed: 2.0
homing_retract_dist: 3.0

[tmc2209 stepper_z]
uart_pin: gpio9
tx_pin: gpio8
uart_address: 1
interpolate: False
run_current: 0.50
sense_resistor: 0.110
stealthchop_threshold: 500

[extruder]
step_pin: gpio11
dir_pin: !gpio10
enable_pin: !gpio12
rotation_distance: 22.67895 #calibrate this value for your individual printer!!
gear_ratio: 50:10
microsteps: 16
nozzle_diameter: 0.400
filament_diameter: 1.750
heater_pin: gpio23
sensor_type: EPCOS 100K B57560G104F
sensor_pin: gpio27
#control: pid
#pid_Kp: 22.2
#pid_Ki: 1.08
#pid_Kd: 114
min_temp: 0
max_temp: 300
min_extrude_temp: 180
max_extrude_only_distance: 150.0
max_extrude_cross_section: .8
Pressure_advance: 0.04655
pressure_advance_smooth_time: 0.040

[tmc2209 extruder]
uart_pin: gpio9
tx_pin: gpio8
uart_address: 0
interpolate: False
run_current: 0.25
sense_resistor: 0.110
stealthchop_threshold: 5000

[heater_bed]
heater_pin: gpio20
sensor_type: Generic 3950
#sensor_type: NTC 100K MGB18-104F39050L32
sensor_pin: gpio26
smooth_time: 3.0
max_power: 0.8
#control: pid
min_temp: 0
max_temp: 120
#pid_kp: 58.437
#pid_ki: 2.347
#pid_kd: 363.769

[printer]
kinematics: corexy
max_velocity: 500
max_accel: 10000
max_z_velocity: 15
max_z_accel: 45
square_corner_velocity: 6.0

[heater_fan hotend_fan]
pin: gpio17
max_power: 1.0
kick_start_time: 0.5
heater: extruder
heater_temp: 50.0
#If you are experiencing back flow, you can reduce fan_speed
#fan_speed: 1.0

[fan]
pin: gpio18
kick_start_time: 0.5
#depending on your fan, you may need to increase or reduce this value
#if your fan will not start
off_below: 0.13
cycle_time: 0.010

[idle_timeout]
timeout: 1800

[homing_override]
axes: z
set_position_z: 0
gcode:
   G90
   G0 Z5 F600
   G28 X Y
   SS_TAKE_PROBE #Deploy Probe
   G1 X40 Y60 F4000 #Move to center of bed
   G28 Z0 #Home z with probe
   SS_STOW_PROBE #Retract Probe

# Tool to help adjust bed leveling screws. One may define a
# [bed_screws] config section to enable a BED_SCREWS_ADJUST g-code
# command.
[bed_screws]
screw1: 60,5
screw1_name: front screw
screw2: 5,115
screw2_name: back left
screw3: 115,115
screw3_name: back right

[force_move]
enable_force_move: True

#####################################################################
#	Macros
#####################################################################

[gcode_macro PRINT_START]
#   Use PRINT_START for the slicer starting script - please customize for your slicer of choice
gcode:
    {% set BED_TEMP = params.BED_TEMP|default(60)|float %}
    {% set HOTEND_TEMP = params.HOTEND_TEMP|default(205)|float %}
    {% set Z_OFFSET = params.Z_OFFSET|default(-0.1)|float %}
    M140 S{BED_TEMP} ; set bed temp
    M190 S{BED_TEMP} ; wait for bed temp
    M104 S{HOTEND_TEMP} ; set extruder temp
    G28
    BED_MESH_PROFILE LOAD=default
    SET_GCODE_OFFSET z={Z_OFFSET}
    G90    
    G0 X0 Y0 Z0.4 F6000
    G0 Z1 F6000
    M109 S{HOTEND_TEMP} ; wait for extruder temp

    G92 E0 ;Reset Extruder
    G0 Z2.0 F3000 ;Move Z Axis up
    G0 X1.5 Y10 Z0.28 F6000.0 ;Move to start position
    G1 X1.5 Y100 Z0.28 F1000.0 E10 ;Draw the first line
    G0 X1.5 Y100 Z0.28 F5000.0 ;Move to side a little
    G1 X1.5 Y10 Z0.28 F1000.0 E15 ;Draw the second line
    G0 Z5.3 F3000;move the nozzle up a little
    G92 E0 ;Reset Extruder
   
[gcode_macro PRINT_END]
#   Use PRINT_END for the slicer ending script - please customize for your slicer of choice
gcode:
    M400                           ; wait for buffer to clear
    G92 E0                         ; zero the extruder
    G1 E-7.0 F3600                 ; retract filament
    G91                            ; relative positioning

    #   Get Boundaries
    {% set max_x = printer.configfile.config["stepper_x"]["position_max"]|float %}
    {% set max_y = printer.configfile.config["stepper_y"]["position_max"]|float %}
    {% set max_z = printer.configfile.config["stepper_z"]["position_max"]|float %}

    #   Check end position to determine safe direction to move
    {% if printer.toolhead.position.x < (max_x - 20) %}
        {% set x_safe = 20.0 %}
    {% else %}
        {% set x_safe = -20.0 %}
    {% endif %}

    {% if printer.toolhead.position.y < (max_y - 20) %}
        {% set y_safe = 20.0 %}
    {% else %}
        {% set y_safe = -20.0 %}
    {% endif %}

    {% if printer.toolhead.position.z < (max_z - 2) %}
        {% set z_safe = 2.0 %}
    {% else %}
        {% set z_safe = max_z - printer.toolhead.position.z %}
    {% endif %}

    G0 Z{z_safe} F3600             ; move nozzle up
    G0 X{x_safe} Y{y_safe} F20000  ; move nozzle to remove stringing
    TURN_OFF_HEATERS
    M107                           ; turn off fan
    G90                            ; absolute positioning
    G0 X60 Y{max_y} F3600          ; park nozzle at rear
	
[gcode_macro LOAD_FILAMENT]
gcode:
   M83                            ; set extruder to relative
   G1 E30 F300                    ; load
   G1 E15 F150                    ; prime nozzle with filament
   M82                            ; set extruder to absolute
    
[gcode_macro UNLOAD_FILAMENT]
gcode:
   M83                            ; set extruder to relative
   G1 E10 F300                    ; extrude a little to soften tip
   G1 E-40 F1800                  ; retract some, but not too much or it will jam
   M82                            ; set extruder to absolute

#################### side swipe stuff

[probe]
pin: ^gpio22 #E0-Stop Connection on SKR mini E3 V2
z_offset: 2.389 #Measure per your specific setup
x_offset: 20
y_offset: 0
speed: 3.0
samples: 2
samples_tolerance_retries: 6

[bed_mesh]
horizontal_move_z: 5
speed: 120
mesh_min: 4,2
mesh_max: 109, 89
probe_count: 3,3
#algorithm: bicubic
relative_reference_index: 4 #Varies based on probe count for 3x3 = 4, for 5x5 = 12

[servo probeServo]
pin: gpio29 #Probe Connection on SKR mini E3 V2
initial_angle: 8
maximum_servo_angle = 180
minimum_pulse_width = 0.0005
maximum_pulse_width = 0.00255

[gcode_macro SS_PICKUP_POS]
variable_x: 120
variable_y: 50
variable_z: 20
gcode:
  M118 pickup pos X:{printer["gcode_macro SS_PICKUP_POS"].x} Y:{printer["gcode_macro SS_PICKUP_POS"].y} Z:{printer["gcode_macro SS_PICKUP_POS"].z}


[gcode_macro SS_DEPLOY]
gcode:
    SET_SERVO SERVO=probeServo ANGLE=98
    G4 P500 # wait for deploy
    #SET_SERVO SERVO=probeServo WIDTH=0 # OFF
    
[gcode_macro SS_RETRACT]
gcode:
    SET_SERVO SERVO=probeServo ANGLE=8
    G4 P500 # wait for retract
    SET_SERVO SERVO=probeServo WIDTH=0 # OFF

[gcode_macro SS_TAKE_PROBE]
gcode:
    G1 Z{printer["gcode_macro SS_PICKUP_POS"].z} F5000
    G1 X{printer["gcode_macro SS_PICKUP_POS"].x - 20} Y{printer["gcode_macro SS_PICKUP_POS"].y} F5000
    SS_DEPLOY
    G1 X{printer["gcode_macro SS_PICKUP_POS"].x}
    G1 Y{printer["gcode_macro SS_PICKUP_POS"].y + 50}
    G1 X{printer["gcode_macro SS_PICKUP_POS"].x - 20}
    SS_RETRACT

[gcode_macro SS_STOW_PROBE]
gcode:
    G1 Z{printer["gcode_macro SS_PICKUP_POS"].z} F5000
    G1 X{printer["gcode_macro SS_PICKUP_POS"].x - 20} Y{printer["gcode_macro SS_PICKUP_POS"].y + 50} F5000
    SS_DEPLOY
    G1 X{printer["gcode_macro SS_PICKUP_POS"].x}
    G1 Y{printer["gcode_macro SS_PICKUP_POS"].y} F2000
    G1 X{printer["gcode_macro SS_PICKUP_POS"].x - 20} F5000
    SS_RETRACT

[gcode_macro AUTO_BED_MESH]
gcode:
    HOME_IF_NEEDED
    Query_Probe
    SS_TAKE_PROBE
    BED_MESH_CALIBRATE
    SS_STOW_PROBE
    SAVE_CONFIG

[gcode_macro HOME_IF_NEEDED]
gcode:
    {% if not "xyz" in printer.toolhead.homed_axes %}
        G28
    {% endif %}

[gcode_macro LOAD_FILAMENT_250]
gcode:
    LOAD_FILAMENT TEMP=250

[gcode_macro UNLOAD_FILAMENT_250]
gcode:
    UNLOAD_FILAMENT TEMP=250

[gcode_macro UNLOAD_FILAMENT]
gcode:
    {% set RETRACT = params.RETRACT|default(80)|float %}
    {% set TEMP = params.TEMP|default(220)|float %}
    M117 Starting UNLOAD_FILAMENT
    #set extruder temp
    M104 S{TEMP}
    #home
    HOME_IF_NEEDED
    #move bed down
    G0 Y0 X60 F4000
    G0 Z110
    #wait for extuder temp
    M117 Waiting for Hotend
    #M109 S{TEMP}
    TEMPERATURE_WAIT SENSOR=extruder minimum={TEMP|int-3}
    #relative extruder
    M83
    #extrude to reduce stringing
    G1 E10 F300                    ; extrude a little to soften tip
    #retract 65mm
    G0 E-{RETRACT} F900
    #absolute mode extruder
    M82
    M117 Finished, start loading!

[gcode_macro LOAD_FILAMENT]
gcode:
    {% set EXTRUDE = params.EXTRUDE|default(150)|float %}
    {% set RETRACT = params.RETRACT|default(7)|float %}
    {% set TEMP = params.TEMP|default(220)|float %}
    #wait for extuder temp
    M117 Waiting for Hotend
    M109 S{TEMP}
    M117 Loading Filament!
    #relative extruder
    M83
    #extrude 
    G0 E{EXTRUDE} F300
    #retract at 20mm/s
    G0 E-{RETRACT} F1200
    #absolute mode extruder
    M82
    #turn off extruder
    M104 S0   
    M117 Finished Loading!

[mcu rpi]
serial: /tmp/klipper_host_mcu

[adxl345]
cs_pin: rpi:None

[resonance_tester]
accel_chip: adxl345
probe_points:
    60,59,20  # an example

[input_shaper]
shaper_freq_x: 80.6
shaper_type_x: mzv
shaper_freq_y: 80.0
shaper_type_y: ei

##   Sensor Types
##   "Trianglelab NTC100K B3950" (Beta 3950 used in LDO kits)
##   "EPCOS 100K B57560G104F"
##   "ATC Semitec 104GT-2"
##   "NTC 100K beta 3950"
##   "Honeywell 100K 135-104LAG-J01"
##   "NTC 100K MGB18-104F39050L32" (Keenovo Heater Pad)
##   "AD595"
##   "PT100 INA826"
##   "PT1000"
##   For more information: https://www.klipper3d.org/Config_Reference.html#temperature_sensor

#*# <---------------------- SAVE_CONFIG ---------------------->
#*# DO NOT EDIT THIS BLOCK OR BELOW. The contents are auto-generated.
#*#
#*# [extruder]
#*# control = pid
#*# pid_kp = 20.756
#*# pid_ki = 1.090
#*# pid_kd = 98.853
#*#
#*# [heater_bed]
#*# control = pid
#*# pid_kp = 42.860
#*# pid_ki = 2.401
#*# pid_kd = 191.262
#*#
#*# [stepper_z]
#*# position_endstop = 0.012
#*#
#*# [bed_mesh default]
#*# version = 1
#*# points =
#*# 	  0.025000, -0.212500, -0.185000
#*# 	  0.072500, 0.000000, 0.103750
#*# 	  0.098750, 0.117500, 0.066250
#*# tension = 0.2
#*# min_x = 4.0
#*# algo = lagrange
#*# y_count = 3
#*# mesh_y_pps = 2
#*# min_y = 2.0
#*# x_count = 3
#*# max_y = 89.0
#*# mesh_x_pps = 2
#*# max_x = 109.0
